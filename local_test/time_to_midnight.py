# filename: time_to_midnight.py
from datetime import datetime

# Get the current time
now = datetime.now()

# Calculate the time difference between the current time and midnight
midnight = datetime(now.year, now.month, now.day, 23, 59, 59)
time_diff = midnight - now

# Print the current time and the number of hours until midnight
print("Current time:", now.strftime("%Y-%m-%d %H:%M:%S"))
print("Hours until midnight:", time_diff.seconds // 3600)