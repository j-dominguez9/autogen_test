# filename: get_boston_time.py
import subprocess

# Get the current time in Boston
command = "TZ='America/New_York' date"
process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
output, error = process.communicate()

# Print the current time in Boston
if error is None:
    print("Current time in Boston is:", output.decode().strip())
else:
    print("Error occurred while fetching time:", error.decode())