# filename: number_letters_multiply.py

# Define a dictionary that maps numbers to their spelling as words
number_words = {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five"
}

# Define the numbers given in the problem
numbers = [3, 4, 5]

# Initialize the product to 1
product = 1

# For each number, get the first letter of its spelling and multiply it to the product
for number in numbers:
    word = number_words[number]
    letter = word[0]
    product *= ord(letter)

# Print the final product
print(product)