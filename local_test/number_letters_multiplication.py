# filename: number_letters_multiplication.py

# Define a dictionary that maps numbers to their spelling
number_spelling = {
    0: "zero",
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine"
}

# Get the first letters of the spelling of each number
number_letters = [number_spelling[num][0] for num in [3, 4, 5]]

# Multiply the letters together
result = 1
for letter in number_letters:
    result *= ord(letter)

# Print the result
print(f"The result is {result}.")