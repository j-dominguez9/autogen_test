# filename: time_in_new_york.py
from datetime import datetime, timezone

# Get the current time in New York
ny_time = datetime.now(timezone(datetime.timedelta(hours=-5)))

# Print the current time in New York
print("Current time in New York:", ny_time)

# Calculate the number of hours until midnight
hours_until_midnight = (24 - ny_time.hour) - ny_time.minute/60 - ny_time.second/3600

# Print the number of hours until midnight
print("Number of hours until midnight in New York:", round(hours_until_midnight, 2))