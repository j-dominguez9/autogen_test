import json
from dotenv import load_dotenv
import os
import autogen
import openai


load_dotenv()

config_list = [
    {
        "model": "mistralai/Mixtral-8x7B-Instruct-v0.1",
        "api_type": "open_ai",
        "base_url": "https://api.endpoints.anyscale.com/v1",
        "api_key": os.getenv("ANYSCALE_API"),
        "cache_seed": None,
    }
]

llm_config = {
    "timeout": 600,
    "cache_seed": 42,
    "config_list": config_list,
    "temperature": 0,
    "max_tokens": 2000,
}


assistant = autogen.AssistantAgent(
    name="assistant",
    llm_config=llm_config,
    is_termination_msg=lambda x: (
        True if "TERMINATE" in x.get("content") else False
    ),
)

user_proxy = autogen.UserProxyAgent(
    name="user_proxy",
    human_input_mode="TERMINATE",
    max_consecutive_auto_reply=1,
    is_termination_msg=lambda x: x.get("content", "")
    and x.get("content", "").rstrip().endswith("TERMINATE"),
    code_execution_config={"work_dir": "./local_test", "use_docker": False},
    llm_config=llm_config,
    system_message="""Reply TERMINATE if the task has been solved at full satisfaction.
Otherwise, reply CONTINUE, or the reason why the task is not solved yet.""",
)

user_proxy.initiate_chat(
    assistant,
    message="""Given the numbers 3, 4, and 5. Take the first letters of the alphabet from each spelling of each number and multiply them together.""",
)
