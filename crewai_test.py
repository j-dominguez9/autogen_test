import os
import openai
from crewai import Task, Crew, Process, Agent
from langchain_community.tools import DuckDuckGoSearchRun
from dotenv import load_dotenv
from langchain_community.tools import WikipediaQueryRun
from langchain_community.utilities import WikipediaAPIWrapper

# from langchain_mistralai.chat_models import ChatMistralAI
# from llama_index.llms.mistralai import MistralAI

load_dotenv()

topic = (
    "Why should I choose Southern Methodist University to learn Data Science?"
)

os.environ["OPENAI_API_BASE"] = "https://api.endpoints.anyscale.com/v1"
os.environ["OPENAI_MODEL_NAME"] = "mistralai/Mixtral-8x7B-Instruct-v0.1"
os.environ["OPENAI_API_KEY"] = os.getenv("ANYSCALE_API")

# llm = MistralAI(api_key=os.getenv("MISTRAL_API"), model="mistral-medium")


researcher = Agent(
    role="Researcher",
    goal=f"Find relevant information about {topic}",
    verbose=True,
    backstory="""Driven by curiosity, you are eager to find information.""",
    # llm=llm,
)

# Writer agent
writer = Agent(
    role="Writer",
    goal=f"Generate report about {topic}",
    verbose=True,
    backstory="""With a flair for simplifying complex topics, you craft engaging narratives that captivate and educate, bringing new discoveries to light in an accessible manner.""",
    # llm=llm,
)


search_tool = DuckDuckGoSearchRun()

wiki_tool = WikipediaQueryRun(api_wrapper=WikipediaAPIWrapper())


research_task = Task(
    description=f"""Find relevant information about {topic}.
    Your final report should clearly articulate the key points.""",
    expected_output="A comprehensive 3 paragraphs long report.",
    max_inter=3,
    tools=[search_tool, wiki_tool],
    agent=researcher,
)

write_task = Task(
    description=f"""Compose an insightful response on {topic}.
    This response should be easy to understand, engaging and positive.""",
    expected_output=f"A two paragraph report on {topic}.",
    tools=[search_tool],
    agent=writer,
)

crew = Crew(
    agents=[researcher, writer],
    tasks=[research_task, write_task],
    process=Process.sequential,
)

result = crew.kickoff()
print(result)
